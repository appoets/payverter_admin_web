<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Encryptable;

class Merchant extends Model
{
    use HasFactory, SoftDeletes, Encryptable;

    protected $encryptable = [
        'first_name', 'last_name', 'email', 'username', 'country_code', 'phone_number'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'password', 'country_code', 'phone_number', 'country_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at',
    ];

    public function country() {
        return $this->belongsTo('App\Models\Country');
    }
}
