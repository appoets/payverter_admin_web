@extends('admin.layouts.app')

@section('content')
<div class="dashboard-content-bdy p-3">
    <div class="card-header py-3 no-bg bg-transparent d-flex align-items-center px-0 justify-content-between border-bottom flex-wrap">
        <h3 class="fw-bold mb-0">Edit Category</h3>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="payout-data">
                <div class="card mb-3">
                    <div class="card-body">
                        <form id="basic-form" method="post" action="{{route('category.update', $category->id)}}" novalidate="">
                            @csrf
                            @method('PATCH')
                            <div class="row g-3 align-items-center">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Name</label>
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{$category->name}}" required>
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mt-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection