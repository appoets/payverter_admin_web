<div class="top-sec">
              <div class="row align-items-center">
                  <div class="col-md-4">
                      <div class="page-title">
                          <h1></h1>
                      </div>
                       <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                  </div>
                  <div class="col-md-8">
                      <div class="right-mnu ">
                          <ul class="d-flex justify-content-end align-items-center">
                              {{--<li>
                                  <div class="toggle-btn">
                                      <span class="t-active">Test</span>
                                      <span>Live</span>
                                  </div>
                              </li>--}}
                              <li>
                                  <div class="profile-show">
                                    <div class="d-flex align-items-center">
                                      <div class="profile-img"><img src="/assets/images/profile-img.png"></div>
                                      <div class="user-name">
                                          <h4>{{Auth::user()->name}}</h4>
                                          <p>Admin</p>
                                      </div>
                                      <div class="select-drop">
                                          <div class="dropdown">
                                          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                            <i class="fal fa-chevron-down"></i>
                                          </button>
                                          <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                  </div>
                              </li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>  