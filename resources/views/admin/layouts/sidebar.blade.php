<nav id="sidebar">
    <div class="sidebar-header admin-logo">
        <img src="/assets/images/logo.png">
    </div>

    <ul class="list-unstyled components">
      
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="{{(Request::segment('1') == 'merchants') ? 'active' : ''}}">
            <a href="{{url('merchants')}}">Merchant</a>
        </li>
        <li class="{{(Request::segment('1') == 'category') ? 'active' : ''}}">
            <a href="{{route('category.index')}}">Category</a>
        </li>
        <li class="{{(Request::segment('1') == 'business-type') ? 'active' : ''}}">
            <a href="{{route('business-type.index')}}">Business Type</a>
        </li>
        <li class="{{(Request::segment('1') == 'business-document') ? 'active' : ''}}">
            <a href="{{route('business-document.index')}}">Business Document</a>
        </li>
        <li class="{{(Request::segment('1') == 'role') ? 'active' : ''}}">
            <a href="{{route('role.index')}}">Role</a>
        </li>
        <li>
            <a href="#">Transactions</a>
        </li>
        <li><a href="#">API Analytics</a></li>
        <li><a href="#">Reports</a></li>
    </ul>
</nav>