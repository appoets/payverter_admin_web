@extends('admin.layouts.app')

@section('content')
    <div class="dashboard-content-bdy p-3">
        <div class="card-header py-3 no-bg bg-transparent d-flex align-items-center px-0 justify-content-between border-bottom flex-wrap">
            <h3 class="fw-bold mb-0">Merchants</h3>
        </div>
             <div class="container-fluid">
                {{--<div class="transection-history-detail merchant-detail">
                    <ul class="d-flex justify-content-between text-center">
                        <li class="d-flex align-items-end">
                            <div class="text-left">
                            <p>Total Merchants</p>
                            <strong>1200</strong>
                        </div>
                        <div class="text-right">
                              <div class="add-merchant">
                                <a href="#" data-toggle="modal" data-target="#add-merchant-form">Add new Merchant</a>
                            </div>
                        </div>
                        </li>
                        <li>
                            <p>Active Merchant</p>
                            <strong>1080</strong>
                        </li>
                        <li>
                            <p>Inactive Merchant</p>
                            <strong>119</strong>
                        </li>
                        <li>
                            <p>Disabled Merchant</p>
                            <strong>1</strong>
                        </li>
                        
                    </ul>
                </div>--}}
               <div class="row">
                 <div class="col-md-12 col-lg-12">
                    {{--<div class="filter-merchant">
                        <ul class="d-flex justify-content-between">
                            <li class="flt-mercnt">
                                <select class="form-control">
                                    <option>All Merchant</option>
                                    <option>Active Merchant</option>
                                    <option>Inactive Merchant</option>
                                </select>
                            </li>
                            <li class="export-btn">
                                <button class="primary-button">Export</button>
                            </li>
                        </ul>
                    </div>--}}
                   <div class="payout-data">
                     <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>S/N</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Status</th>
                <th>Actions</th>
                
            </tr>
        </thead>
        <tbody>
            @forelse($merchants as $index => $merchant)
            <tr>
                <td>{{$index+1}}</td>
                <td>{{$merchant->first_name.' '.$merchant->last_name}}</td>
                <td>{{$merchant->country_code.'-'.$merchant->phone_number}}</td>
                <td>{{$merchant->email}}</td>
                <td>@if($merchant->status == 'ACTIVE')<button class="active-m">Active</button>@else<button class="deactive-m">Inactive</button>@endif</td>
                <td><div class="dropdown">
                  <button class="btn  merchant-action dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-ellipsis-h"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </div>
                </div>
              </td>
           </tr>
           @empty
           <tr>
               <td colspan="6"><p class="text-center">No results found.</p></td>
           </tr>
           @endforelse
          </tbody>
        <tfoot>
            <tr>
               <th>S/N</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
                   </div>
                   <hr>
             
                 </div>
               </div>
             </div> 
          </div>


          <div id="add-merchant-form" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
   
      <div class="modal-body">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2>Add Merchant</h2>
        <div class="merchant-add-form">
            <div class="form-group">
                <label>Full name</label>
                <input type="text" name="" class="form-control" placeholder="Enter your full name">
            </div>
            <div class="form-group">
                <label>Business Name</label>
                <input type="text" name="" class="form-control" placeholder="Enter your full name">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="" class="form-control" placeholder="Enter your email">
            </div>
            <div class="form-group">
                <label>Phone Number</label>
                <input type="tel" name="" class="form-control" placeholder="Enter your Phone Number">
            </div>
            <div class="form-group">
                <label>Role</label>
                <input type="text" name="" class="form-control" placeholder="Enter a role for this merchant">
            </div>
            <div class="form-group text-center">
                <button type="button" class="primary-button">Add Merchants</button>
            </div>
        </div>
      </div>
    
    </div>

  </div>
</div>

@endsection

@section('style')

<link rel="stylesheet" type="text/css" href="/assets/css/datatable.css">

@endsection

@section('script')

<script type="text/javascript" src="/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>

@endsection