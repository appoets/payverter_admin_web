@extends('admin.layouts.app')

@section('content')
<div class="dashboard-content-bdy p-3">
    <div class="card-header py-3 no-bg bg-transparent d-flex align-items-center px-0 justify-content-between border-bottom flex-wrap">
        <h3 class="fw-bold mb-0">Business Document</h3>
        <div class="col-auto d-flex w-sm-100">
        <a href="{{route('business-document.create')}}" class="btn btn-primary btn-set-task w-sm-100" data-bs-toggle="modal" data-bs-target="#sendsheet">Create</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="payout-data">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($businessDocuments as $index => $businessDocument)
                        <tr>
                            <td>{{$index+1}}</td>
                            <td>{{$businessDocument->name}}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn  merchant-action dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="{{route('business-document.edit', $businessDocument->id)}}" class="dropdown-item">Edit</a>
                                        <a href="#" class="dropdown-item" onClick="event.preventDefault(); if(confirm('Are you sure want to delete?')) { document.getElementById('business-document-delete-{{$businessDocument->id}}').submit();}" class="btn btn-outline-secondary deleterow">Delete</a>
                                        <form id="business-document-delete-{{$businessDocument->id}}" action="{{ route('business-document.destroy', $businessDocument->id) }}" method="POST" class="d-none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="3"><p class="text-center">No results found.</p></td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S/N</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
                {!!$businessDocuments->render()!!}
            </div>
            <hr>
        </div>
    </div>
</div>

@endsection

@section('style')

<link rel="stylesheet" type="text/css" href="/assets/css/datatable.css">

@endsection

@section('script')

<script type="text/javascript" src="/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>

@endsection