<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Payvettar Admin</title>

    <!-- Bootstrap CSS CDN -->
     <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="/assets/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/admin.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/responsive.css">
    <!-- Font Awesome JS -->
  

</head>
<body>

<div class="makepayment admin-page admin-login">
    <div class="makepayment-form ">
        <form method="POST" action="{{ route('login') }}">
            @csrf()
            <div class="logo"><img src="/assets/images/logo-full.png"></div>
         
            <div class="form-sec-col ">
                <h2>Login</h2>
                <p>Enter details below to login</p>
                <div class="form-group">
                    <label for="email">Email Address</label>
                    <input type="email" name="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="email" placeholder="Enter email address" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group password">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            
                <hr>
                <div class="form-group sub-button">
                    <button type="submit" class="btn btn-default primary-button" >Login</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="/assets/js/jquery.min.js"></script>
  <script src="/assets/js/popper.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/assets/js/Chart.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function()
            {
                $('.toggle-btn span').click(function()
                    {
                        $(this).siblings().removeClass('t-active');
                        $(this).addClass('t-active');
                    });
            });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.form-group.password i').click(function() {
                $('.form-group.password').toggleClass('show-password');
                $('input#pwd').attr('type','password');
                $('.show-password input#pwd').attr('type','text');
                $('.password i.fa.fa-eye-slash').attr('class','fa fa-eye');
                $('.show-password i.fa.fa-eye').attr('class','fa fa-eye-slash');
            });
        });
    </script>
    
</body>

</html>