<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // return view('welcome');
//     return view('admin.login');
// });

Route::get('/', 'App\Http\Controllers\Auth\LoginController@showLoginForm');

Auth::routes();

Route::post('/login', 'App\Http\Controllers\Auth\LoginController@login');

// Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');

Route::get('/merchants', 'App\Http\Controllers\Resource\MerchantResource@index');

Route::resource('category', 'App\Http\Controllers\Resource\CategoryResource');

Route::resource('business-type', 'App\Http\Controllers\Resource\BusinessTypeResource');

Route::resource('business-document', 'App\Http\Controllers\Resource\BusinessDocumentResource');

Route::resource('role', 'App\Http\Controllers\Resource\RoleResource');
